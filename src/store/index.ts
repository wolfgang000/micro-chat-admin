import Vue from 'vue'
import Vuex from 'vuex'
import accounts from './accounts';
import chat from './chat';


Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    accounts,
    chat
  }
})

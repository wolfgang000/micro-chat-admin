import { IMessage } from '@/models';

export default {
  namespaced: true,
  state: {
    messages: [] as IMessage[],
    ticket: undefined,
    new_message: '',
  },
  mutations: {
    SET_TICKET: (state: any, ticket: any) =>  state.ticket = ticket,
    SET_MESSAGES: (state: any, messages: IMessage[]) => state.messages = messages,
    PUSH_LAST_MESSAGE: (state: any, message: IMessage) => state.messages.unshift(message),
  }
};

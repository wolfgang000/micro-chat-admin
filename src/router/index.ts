import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue';
import Login from '../views/Login.vue';
import Signup from '../views/Signup.vue';
import Dashboard from '../views/home/Dashboard.vue';
import TicketsView from '../views/home/TicketsView.vue';
import TicketCreateView from '@/views/home/TicketCreateView.vue';
import TicketDetailView from '@/views/home/TicketDetailView.vue';
import { accounts } from '@/api';

Vue.use(VueRouter)
// export const ROUTE_DASHBOARD = 'home.dashboard';
export const ROUTE_TICKET_LIST = 'ticket.list';
export const ROUTE_TICKET_CREATE = 'ticket.create';
export const ROUTE_TICKET_DETAIL = 'ticket.detail';
export const ROUTE_CREATE_SURVEY = 'home.create_survey';
export const ROUTE_API_KEYS = 'settings.api_keys';
export const ROUTE_LOGIN = 'login';
export const ROUTE_SIGNUP = 'signup';


function requireGuest(to: any, from: any, next: any) {
  next(!accounts.isAuthenticated());
}

function requireUser(to: any, from: any, next: any) {
  // will login and come back
  next(accounts.isAuthenticated() ? true : {
    name: ROUTE_LOGIN,
    query: {
      redirect: to.fullPath,
    },
  });
}

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'home',
    redirect: { name: ROUTE_TICKET_LIST },
    beforeEnter: requireUser,
  },
  {
    path: '/home/',
    component: Home,
    beforeEnter: requireUser,
    children: [
      // {
      //   path: 'dashboard',
      //   name: ROUTE_DASHBOARD,
      //   component: Dashboard,
      // },
      {
        path: 'tickets',
        name: ROUTE_TICKET_LIST,
        component: TicketsView,
      },
      {
        path: 'tickets/new',
        name: ROUTE_TICKET_CREATE,
        component: TicketCreateView,
      },
      {
        path: 'tickets/:ticketNumber',
        name: ROUTE_TICKET_DETAIL,
        component: TicketDetailView,
      },
    ],
  },
  {
    path: '/account/login',
    name: ROUTE_LOGIN,
    component: Login,
    beforeEnter: requireGuest,
  },
  {
    path: '/account/signup',
    name: ROUTE_SIGNUP,
    component: Signup,
    beforeEnter: requireGuest,
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

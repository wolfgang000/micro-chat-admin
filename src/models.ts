export enum TicketStatus {
  Closed = 1,
  Open = 2
}

export function getTicketStatusStr(status: TicketStatus): string {
  switch(status) {
    case TicketStatus.Closed:
      return "Closed"
    case TicketStatus.Open:
      return "Open"
    default:
      return "";
  }
}

export interface ITicket {
  id: number;
  number: number;
  subject: string;
  status: TicketStatus;
  assigned_agent_id: number;
  contact: {
    email: string;
  };
}

export interface IMessage {
  id: number;
  user_organization_id: number;
  inserted_at: string;
}

export function GenNullTicket(): ITicket {
  return {
    id: -1,
    number: -1,
    subject: '',
    status: TicketStatus.Open,
    assigned_agent_id: -1,
    contact: {
      email: ''
    }
  };
}
